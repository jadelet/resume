### My Resume
#### The idea behind this resume "project" is to show that this attorney/contracts manager can put together a simple HTML project in a repo, has a basic understanding of markdown language, and is quite adaptable and trainable in the IT space. 

**Please note, I have set my resume below in Markdown, and also have included in this repository an HTML version. I can certainly provide a word version upon request, as needed.**

---
<div align="center">

# JENNIFER N. SHEIDY
</div>
<div align="center"> 

##  Old Bridge, NJ
</div>

<div align="center">

Phone 973-454-7072 * E-mail: jadelet@hotmail.com
</div>


### SUMMARY:
Experienced attorney with a broad range of industry exposure in contract review and management and commercial law with and a focus in Life Sciences and Information Technology.

## RELEVANT EXPERIENCE: 

### ATTORNEY (TEMP-TO-PERM AT ICIMS)  		Holmdel, NJ		  	        July 2019 to August 2020

Commercial attorney for an IT provider of HR SaaS. 
-	Contract drafting, negotiation as well as commercial legal advice for both Sales and Procurement.
-	Eliminated backlog, brought all contracts to current turnaround goals, moved business forward with creative negotiations. 
-	Worked with SaaS Agreements and DPAs, as well as the broad variety of vendor agreements typically associated with an IT company.

### IT CONTRACTS MANAGER 	(TEMP AT BRISTOL MYER SQUIBB) Raritan, NJ    	          March 2019 to July 2019 

- Researched, analyzed and categorized IT contracts for divestiture.  
- Analyzed contracts and researched best practices for IT Licensing and Software as a Service. 

### ATTORNEY FOR J. KNIPPER AND COMPANY, INC.	Somerset/Lakewood, NJ		   Jan 2016 to Sept 2018

- Commercial attorney for a Pharmaceutical Services company with a focus on sample tracking and distribution technology. Contract drafting, negotiation as well as legal advice and materials review for the various business groups.  
- Implemented new contracts management database; responsible for data transfer coordination and upload, creating structure and organization of customized database, trained team on database, sole POC to database vendor, helped structure and create internal departmental best practices guides. Developed methods to maximize customized modules to establish smooth, reportable data gathering and contracts storage. 

### ATTORNEY FOR AXIOM (TEMP AT HIKMA US.) 	Eatontown, NJ	   			May 2014 to Jan 2016

- Commercial attorney for a generic pharmaceutical manufacturer under supervision of VP of Legal Affairs. Supported both sales and marketing / purchasing units. Created a bank of in-house contract templates, eliminated contract backlog, aided in selection and implementation of contract management system. Provided support for M&A projects. 

### ATTORNEY (TEMP AT CATALENT PHARMACEUTICALS)  	 Princeton,NJ       		May 2013 to Jan 2014

- Taken on as a supplemental project attorney for Catalent, a previous employer. Performed database scrub and integration of new contracts from a recent acquisition, and handled contract review of major vendor contract renewal. 

### SOURCING MANAGER-IT (TEMP AT JOHNSON & JOHNSON ITS)		 Raritan, NJ    	Sept 2012 to May 2013 

- Drafted, negotiated, reviewed vendor contracts for the IT division of a major international pharmaceutical company.
- Handled master agreements, licensing agreements, statements of work, work orders, and confidentiality agreements.
- Handled RFP’s, coordinated legal and business reviews, provided gap analysis for resources, oversaw various projects.
 

### CONTRACTS MANAGER FOR AGENNIX INC. 		Princeton, NJ			  	Sept 2011 to Aug 2012

- Drafted and negotiated contracts for an international startup biotech company under the Director of Business Development. 
- Responsible for all confidentiality agreements, consulting agreements, service agreements, IT agreements, and HR agreements; also worked on material transfer agreements and clinical trial agreements under supervision. 
- Implemented new contracts management database; responsible for data transfer coordination and upload, trained team on database, liaison to database vendor, helped structure and create internal departmental best practices guides. 

### PROCUREMENT CONTRACTS LEAD FOR CATALENT PHARMA SOLUTIONS, LLC Somerset, NJ	  Aug 2009 to Sept 2011 

- Drafted, negotiated, reviewed, and maintained procurement contracts for a multinational pharmaceutical solutions provider under supervision of Assistant GC, also assisted all business units in drafting commercial contracts where needed. 

### E-DISCOVERY ATTORNEY FOR VARIOUS FIRMS, NJ/NY 					Oct 2007 to August 2009

- Document review for various firms, included pharma, securities and white-collar criminal litigation.
- Selected for special teams (2nd level review) on several occasions.

### CONTRACTS ADMINISTRATOR FOR ARBINET-THEXCHANGE, INC. 	New Brunswick, NJ 	May 2006 to Sept 2007 

- Drafted, negotiated, reviewed, tracked, administered, and monitored contracts for a major telecommunications exchange.
- Types of contracts included Telecom Master Service Agreements, as well as vendor agreements, real estate agreements, purchase agreements, and IT contracts. 
- Litigation support, document retention, and aid with corporate governance and patent filings. 

### TEMPORARY ATTORNEY FOR CHARLES SHEIDY ATTORNEY		Denver, PA	             Jan 2006 to May 2006	
- Briefly took over and managed father’s private practice while he recovered from quadruple bypass. Made bankruptcy court appearances, took client meetings, drafted QDRO, attended and represented  2 townships in municipal meetings.

### COMPLIANCE SPECIALIST/PARALEGAL FOR CENDANT 	Parsippany, NJ		  September 2003 to June 2005 

- Monitored hotel franchise/license compliance with terms of the franchise/license agreement.
- Drafted defaults, amendments, termination agreements, and correspondence for international sites.
- Investigated franchisee issues, devised unique win-win solutions designed to keep licensees in good standing.

## EDUCATION 

### RUTGERS UNIVERSITY CONTINUING EDUCATION   							Somerset, NJ
Certificate, Full-stack Programming from Rutgers Coding Bootcamp

### SETON HALL UNIVERSITY SCHOOL OF LAW, 								  Newark, NJ
Juris Doctor
- Research Assistant (Professor John Wefing)
- Family Law Clinic

### SHEPHERD COLLEGE										 Shepherdstown, WV
BA Cum Laude English Literature; Music Minor
- Editor-In-Chief of Student Newspaper 
- Senator in Student Government Association

## BAR MEMBERSHIPS 

NJ BAR, 2001-PRESENT, IN GOOD STANDING

